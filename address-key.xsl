<?xml version="1.0" encoding="utf-8"?>
<!-- Copyright (c) 2005-2010 DeltaXML Ltd. All rights reserved -->
<!-- $Id$ -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns:deltaxml="http://www.deltaxml.com/ns/well-formed-delta-v1">

  <xsl:output method="xml" indent="no" />

  <xsl:template match="*|@*">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="person">
    <xsl:copy>
      <xsl:attribute name="deltaxml:key" select="@customerid"/>
      <xsl:apply-templates select="@*, node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="addressList">
    <xsl:copy>
      <xsl:attribute name="deltaxml:ordered" select="'false'"/>
      <xsl:apply-templates select="@*, node()"/>
    </xsl:copy>
  </xsl:template>


</xsl:stylesheet>
