﻿// Copyright (c) 2015 DeltaXML Ltd.  All rights reserved.

using System;
using System.IO;
using DeltaXML.CoreS9Api;
using com.deltaxml.cores9api.config;
using net.sf.saxon.s9api;

namespace com.deltaxml.samples.OrderlessComparison
{
    class OrderlessComparison
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Orderless Comparison");
            Console.WriteLine("Initializing DocumentComparator");
            DocumentComparator dc = new DocumentComparator();

            dc.setOutputProperty(Serializer.Property.INDENT, "yes");
            dc.setOutputProperty(Serializer.Property.METHOD, "xml");

            dc.ResultReadabilityOptions.ElementSplittingEnabled = false;
            dc.ResultReadabilityOptions.ModifiedWhitespaceBehaviour = ModifiedWhitespaceBehaviour.NORMALIZE;
            dc.OutputFormatConfiguration.OrderlessPresentationMode = OrderlessPresentationMode.B_DELETES;
            dc.LexicalPreservationConfig.PreserveIgnorableWhitespace = false;

            FilterStepHelper fsHelper = dc.newFilterStepHelper();
            FilterChain infilters = fsHelper.newFilterChain();
            infilters.addStep(fsHelper.newSingleStepFilterChain(new FileInfo("meta-section-key.xsl"), "meta-section-key"));
            dc.setExtensionPoint(DocumentComparator.ExtensionPoint.PRE_FLATTENING, infilters);

            FileInfo f1 = new FileInfo(@"..\..\..\..\..\documentA.html");
            FileInfo f2 = new FileInfo(@"..\..\..\..\..\documentB.html");
            FileInfo resultAB = new FileInfo(@"..\..\..\..\..\resultAB.xml");

            Console.WriteLine("Performing the comparison.");
            dc.compare(f1, f2, resultAB);
            Console.WriteLine("Comparison complete. Result is in " + resultAB.FullName);

            FileInfo f3 = new FileInfo(@"..\..\..\..\..\documentC.html");
            FileInfo resultAC = new FileInfo(@"..\..\..\..\..\resultAC.xml");

            Console.WriteLine("Performing the comparison.");
            dc.compare(f1, f3, resultAC);
            Console.WriteLine("Comparison complete. Result is in " + resultAC.FullName);
            Console.WriteLine("Press <ENTER> to continue...");
            Console.ReadLine();



        }
    }
}
