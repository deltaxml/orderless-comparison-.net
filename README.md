# Orderless Comparison (.NET)

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Compare release. The resources should be located such that they are two levels below the top level release directory.*

*For example `DeltaXML-XML-Compare-10_0_0_n/samples/orderless-comparison`.*

---

How and why to instruct XML Compare to treat XML data as an orderless set of elements.

This document describes how to run the sample. For concept details see: [Comparing Orderless Elements](https://docs.deltaxml.com/xml-compare/latest/samples-and-guides/comparing-orderless-elements)

## Running the Sample

### Pipelined Comparator (DXP)

The sample can be run via a run.bat batch file, so long as this is issued from the sample directory. Alternatively, the command can be executed directly:

	..\..\bin\deltaxml.exe compare orderless documentA.xml documentB.xml resultAB.xml
	..\..\bin\deltaxml.exe compare orderless documentA.xml documentC.xml resultAC.xml

### Document Comparator (API)
A Visual Studio solution and project are provided in the *dotnet-api* directory. You can run this in the usual way from Visual Studio (keyboard shortcut: F5).

# **Note - .NET support has been deprecated as of version 10.0.0 **